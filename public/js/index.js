const formAddItem = document.getElementById('formAddItem');
const dateInput = document.getElementById('dateInput');
const addItemModal = document.getElementById('addItemModal');
const modal = new bootstrap.Modal(addItemModal);
const langRu = document.getElementById('langRu');
const langEn = document.getElementById('langEn');
const currencyRub = document.getElementById('currencyRub');
const currencyUsd = document.getElementById('currencyUsd');
const changeLangLink = document.getElementById('changeLang');
const changeCurrencyLink = document.getElementById('changeCurrency');
const currencyTable = document.getElementById('currencyTable');
const tableItems = document.getElementById('tableItems');
const tableHeaders = tableItems.getElementsByTagName('span');
const buttonAdd = document.getElementById('buttonAdd');

addItemModal.addEventListener('show.bs.modal', function (e) {
  const currentTime = new Date();
  dateInput.value = convertToDateTimeLocalString(currentTime);
});

const setLang = (lang) => {
  for (const th of tableHeaders) {
    if (th.id && translateDictionary[lang][th.id]) th.innerHTML = translateDictionary[lang][th.id];
  }
  buttonAdd.innerHTML = translateDictionary[lang]['buttonAdd'];
  document.getElementById('addItemLabel').innerHTML = translateDictionary[lang]['buttonAdd'];
  document.getElementById('modalButtonClose').innerHTML = translateDictionary[lang]['modalButtonClose'];
  document.getElementById('modalButtonAdd').innerHTML = translateDictionary[lang]['modalButtonAdd'];
  document.getElementById('titleInputError').innerHTML = translateDictionary[lang]['titleInputError'];
  document.getElementById('priceInputError').innerHTML = translateDictionary[lang]['priceInputError'];
  document.getElementById('dateInputError').innerHTML = translateDictionary[lang]['dateInputError'];
};

formAddItem.addEventListener(
  'submit',
  (e) => {
    if (!formAddItem.checkValidity()) {
      e.preventDefault();
      e.stopPropagation();
      console.log('Not valid');
    }
    formAddItem.classList.add('was-validated');
    if (formAddItem.checkValidity()) {
      e.preventDefault();
      e.stopPropagation();
      const formData = new FormData(formAddItem);
      const data = Object.fromEntries(formData);
      if (!parseFloat(data.price) || parseFloat(data.price) < 0) {
        const priceInput = document.getElementById('priceInput');
        priceInput.classList.add('is-invalid');
        return;
      }
      console.log('Valid', data);
      saveData(data);
      formAddItem.reset();
      priceInput.classList.remove('is-invalid');
      formAddItem.classList.remove('was-validated');
      getData();
      modal.hide();
    }
  },
  false
);

const clearDataSetDirectionSort = (id) => {
  for (const th of tableHeaders) {
    if (th.id === id) continue;
    th.dataset.directionSort = '';
  }
};

for (const th of tableHeaders) {
  th.addEventListener('click', (e) => {
    if (e.target.id) {
      console.log(e.target.id);
      clearDataSetDirectionSort(e.target.id);
      th.dataset.directionSort = th.dataset.directionSort === 'ASC' ? 'DESC' : 'ASC';
      getData(e.target.id, th.dataset.directionSort);
    }
    return;
  });
}

const getData = (typeSort = '', directionSort = 'ASC') => {
  const items = localStorage.getItem('items');
  const currency = localStorage.getItem('currency');
  const currencyRate = localStorage.getItem('currencyRate');

  if (items) {
    const parsedItems = JSON.parse(items);
    switch (typeSort) {
      case 'headTitle':
        if (directionSort === 'ASC') parsedItems.sort((a, b) => a.title.localeCompare(b.title));
        else parsedItems.sort((a, b) => b.title.localeCompare(a.title));
        break;
      case 'headPrice':
        if (directionSort === 'ASC') parsedItems.sort((a, b) => a.price - b.price);
        else parsedItems.sort((a, b) => b.price - a.price);
        break;
      case 'headDate':
        if (directionSort === 'ASC') parsedItems.sort((a, b) => new Date(a.date) - new Date(b.date));
        else parsedItems.sort((a, b) => new Date(b.date) - new Date(a.date));
        break;
      default:
        parsedItems.sort((a, b) => new Date(a.date) - new Date(b.date));
        break;
    }

    const listItems = document.getElementById('listItems');
    listItems.innerHTML = '';
    if (currency === 'RUB') {
      createTable(parsedItems, currencyRate, '₽');
    } else {
      createTable(parsedItems);
    }
  }
};

const createTable = (data, kurs = 1, simvol = '$') => {
  data.forEach((item, index) => {
    const price = parseFloat(item.price * kurs).toFixed(2);
    const date = new Date(item.date);
    listItems.innerHTML += `
			<tr>
				<th scope="row">${index + 1}</th>
				<td>${item.title}</td>
				<td style="text-align: right;">${price} <span>${simvol}</span></td>
				<td>${date.toLocaleString('ru-RU')}</td>
				<td class="text-center"><button class="btn btn-primary btn-delete-data " onclick="deleteItem(${index})">Del</button></td>
			</tr>`;
  });
};

const deleteItem = (index) => {
  const items = localStorage.getItem('items');
  if (items) {
    const parsedItems = JSON.parse(items);
    parsedItems.splice(index, 1);
    localStorage.setItem('items', JSON.stringify(parsedItems));
    getData();
  }
};

const saveData = (data) => {
  const items = localStorage.getItem('items');
  if (items) {
    const parsedItems = JSON.parse(items);
    parsedItems.push(data);
    localStorage.setItem('items', JSON.stringify(parsedItems));
    return;
  }
  const dataString = JSON.stringify([data]);
  localStorage.setItem('items', dataString);
};

const convertToDateTimeLocalString = (date) => {
  const year = date.getFullYear();
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const day = date.getDate().toString().padStart(2, '0');
  const hours = date.getHours().toString().padStart(2, '0');
  const minutes = date.getMinutes().toString().padStart(2, '0');
  return `${year}-${month}-${day}T${hours}:${minutes}`;
};

const checkSetting = () => {
  const lang = localStorage.getItem('lang');
  const currency = localStorage.getItem('currency');
  if (!lang) {
    localStorage.setItem('lang', 'en');
    langEn.classList.add('activeLang');
    currencyTable.innerHTML = 'USD';
  }
  if (!currency) {
    localStorage.setItem('currency', 'USD');
    currencyUsd.classList.add('activeCurrency');
  }
  if (lang === 'ru') {
    langRu.classList.add('activeLang');
    langEn.classList.remove('activeLang');
  }
  if (lang === 'en') {
    langEn.classList.add('activeLang');
    langRu.classList.remove('activeLang');
  }
  if (currency === 'RUB') {
    currencyRub.classList.add('activeCurrency');
    currencyUsd.classList.remove('activeCurrency');
  }
  if (currency === 'USD') {
    currencyUsd.classList.add('activeCurrency');
    currencyRub.classList.remove('activeCurrency');
  }
  setLang(lang);
  getData();
};

const changeLang = (e) => {
  e.preventDefault();
  const lang = localStorage.getItem('lang');
  const currentLang = lang === 'ru' ? 'en' : 'ru';
  localStorage.setItem('lang', currentLang);
  setLang(currentLang);
  checkSetting();
};

const changeCurrency = (e) => {
  e.preventDefault();
  const currency = localStorage.getItem('currency');
  const currentCurrency = currency === 'RUB' ? 'USD' : 'RUB';
  currencyTable.innerHTML = currentCurrency;
  localStorage.setItem('currency', currentCurrency);
  checkSetting();
};

const getRate = async () => {
  const currencyRateDate = localStorage.getItem('currencyRateDate');
  if (!currencyRateDate || Date.now() - currencyRateDate > 3600000) {
    const url = 'https://www.cbr-xml-daily.ru/daily_json.js';
    const rate = await fetch(url);
    const data = await rate.json();
    localStorage.setItem('currencyRate', data.Valute.USD.Value);
    localStorage.setItem('currencyRateDate', Date.now());
  }
  localStorage.setItem('timeNow', Date.now());
};

changeLangLink.addEventListener('click', changeLang);
changeCurrencyLink.addEventListener('click', changeCurrency);

window.addEventListener('load', (event) => {
  document.getElementById('copyright').innerHTML = 'Создано &copy; IMGusev ' + new Date().getFullYear();
  getRate();
  getData();
  checkSetting();
  console.log('Основной файл загружен');
});
