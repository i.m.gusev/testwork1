const rusLang = {
  headTitle: 'Название',
  headPrice: 'Стоимость',
  headDate: 'Время добавления',
  headAction: 'Управление',
  delete: 'Удалить',
  buttonAddModal: 'Добавить',
  buttonAdd: 'Новый товар',
  created: 'Создано',
  modalButtonClose: 'Закрыть',
  modalButtonAdd: 'Добавить',
  titleInputError: 'Укажите наименование',
  priceInputError: 'Укажите стоимость',
  dateInputError: 'Заполните поле дата',
};

const engLang = {
  headTitle: 'Title',
  headPrice: 'Price',
  headDate: 'Date and time',
  headAction: 'Action',
  delete: 'Del',
  buttonAddModal: 'Add',
  buttonAdd: 'New item',
  created: 'Created',
  modalButtonClose: 'Close',
  modalButtonAdd: 'Add',
  titleInputError: 'Please choose a Title',
  priceInputError: 'Please choose a Price',
  dateInputError: 'Please choose a Date and Time',
};

translateDictionary = {
  ru: rusLang,
  en: engLang,
};

console.log('Языки загружены');
